﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour
{
    public bool facingUp = true;             // For determinging which way the play is oriented.
    public bool jump = false;               // Condition for whether the player should jump.


    public float moveForce = 365f;          // Amount of force added to move the player left and right.
    public float maxSpeed = 5f;             // The fastest the player can travel in the x axis.
    public float jumpForce = 1000f;         // Amount of force added when the player jumps.
    public bool reversed = false;


    private Transform groundCheck;          // A position marking where to check if the player is grounded.
    private bool grounded = false;          // Whether or not the player is grounded.

    private Rigidbody2D rb2d;


    void Awake()
    {
        groundCheck = transform.Find("groundCheck");    // Setting up references.
        rb2d = GetComponent<Rigidbody2D>();

    }


    void Update()
    {
        // The player is grounded if a linecast to the groundcheck position hits anything on the ground layer.
        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

        // If the jump button is pressed and the player is grounded then the player should jump.
        if (Input.GetButtonDown("Jump") && grounded)
            jump = true;

        if (Input.GetButtonDown("Submit"))
            GetComponent<Rigidbody2D>().gravityScale *= -1;
            reversed = !reversed;
           // Rotate();
    }


    void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");

        // anim.SetFloat("Speed", Mathf.Abs(h));

        if (h * rb2d.velocity.x < maxSpeed)
            rb2d.AddForce(Vector2.right * h * moveForce);

        if (Mathf.Abs (rb2d.velocity.x) > maxSpeed)
            rb2d.velocity = new Vector2(Mathf.Sign (rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);

        if (jump && reversed)
        { 
            rb2d.AddForce(new Vector2(0f, (jumpForce * -1)));
            jump = false;
        }
        if (jump && reversed == false)
        {
            rb2d.AddForce(new Vector2(0f, jumpForce));
            jump = false;
        }
    }


    void Rotate()
    {
        // Switch the way the player is oriented
        facingUp = !facingUp;
       // Vector3 theScale = transform.localScale;
        //theScale.y *= -1;
        transform.localScale *= -1; // theScale;
    }

}